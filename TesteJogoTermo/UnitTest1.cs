namespace JogoTermo;

[TestClass]
public class TesteJogoTermo
{
    [TestMethod]
    public void TesteRespondendoVazio()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        Status status = ValidacaoResposta.VerificarPalpite("  ");
        Assert.AreEqual(Status.DigiteUmaPalvra, status);
    }

      [TestMethod]
    public void TesteRespondendoPalavraMenorQue5Letras()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        Status status = ValidacaoResposta.VerificarPalpite("ovo");
        Assert.AreEqual(Status.PalavraNaoPossueCincoLetras, status);
    }

      [TestMethod]
    public void TesteRespondendoPalavraMaiorQue5Letras()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        Status status = ValidacaoResposta.VerificarPalpite("parafuso");
        Assert.AreEqual(Status.PalavraNaoPossueCincoLetras, status);
    }

       [TestMethod]
    public void TesteRespondendoPalavraComPeloMenosUmNumero()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        Status status = ValidacaoResposta.VerificarPalpite("port1");
        Assert.AreEqual(Status.DigiteUmaPalvra, status);
    }

       [TestMethod]
    public void TesteRespondendoPalavraComCaracterEspecial()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        Status status = ValidacaoResposta.VerificarPalpite("port@");
        Assert.AreEqual(Status.DigiteUmaPalvra, status);
        
    }

      [TestMethod]
    public void TesteVenceu()
    {
        GerenciadorDoJogo game1 = new GerenciadorDoJogo();

        List<Status> teste = new List<Status>(){
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk
        };

        Status atual = game1.VenceuOuPerdeu(teste);

        Status Esperado = Status.Venceu;

        Assert.AreEqual(Esperado, atual);
    }

      [TestMethod]
    public void TestePerdeu()
    {
        GerenciadorDoJogo game1 = new GerenciadorDoJogo();

        List<Status> teste = new List<Status>(){
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.LetterExistsInAnotherPosition
        };

        Status atual = game1.VenceuOuPerdeu(teste);

        Status Esperado = Status.Perdeu;

        Assert.AreEqual(Esperado, atual);
    }


         [TestMethod]
    public void TesteAcertandoTodasAsLetras()
    {
        GerenciadorDoJogo game = new GerenciadorDoJogo();
        BancoDePalavras banco = new BancoDePalavras();
        banco.PalavraSorteada = "PERTO";
        BancoDePalavras.ArrayPalavraSorteada = banco.PalavraSorteada.ToCharArray();
        List<Status> actual = game.Jogar("PERTO");
        
        List<Status> expected = new List<Status>(){
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk
        };
        CollectionAssert.AreEqual(expected, actual );
    }

         [TestMethod]
    public void TesteUltimaLetraErradaPosicaoErrada()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        BancoDePalavras banco = new BancoDePalavras();
        banco.PalavraSorteada = "PERTO";
        BancoDePalavras.ArrayPalavraSorteada = banco.PalavraSorteada.ToCharArray();

        List<Status> current = jogo.Jogar("PERTU");
        List<Status> expected = new List<Status>(){
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.PositionAndLetterOk,
            Status.LetterDoesNotExist,     
        };
        CollectionAssert.AreEqual(expected, current );
    }

        [TestMethod]
    public void TesteUltimaLetraCertaMasPosicaoErrada()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        BancoDePalavras banco = new BancoDePalavras();
        banco.PalavraSorteada = "MOSCA";
        BancoDePalavras.ArrayPalavraSorteada = banco.PalavraSorteada.ToCharArray();
        List<Status> current = jogo.Jogar("JUSTO");
        List<Status> expected = new List<Status>(){
            Status.LetterDoesNotExist,
            Status.LetterDoesNotExist,
            Status.PositionAndLetterOk,
            Status.LetterDoesNotExist,
            Status.LetterExistsInAnotherPosition,     
        };
        CollectionAssert.AreEqual(expected, current );
    }

           [TestMethod]
    public void TesteUltimaLetraCertaMasPosicaoErrada2()
    {
        GerenciadorDoJogo jogo = new GerenciadorDoJogo();
        BancoDePalavras banco = new BancoDePalavras();
        banco.PalavraSorteada = "PERTO";
        BancoDePalavras.ArrayPalavraSorteada = banco.PalavraSorteada.ToCharArray();
        List<Status> current = jogo.Jogar("TESTE");
        List<Status> expected = new List<Status>(){
            Status.LetterExistsInAnotherPosition,
            Status.PositionAndLetterOk,
            Status.LetterDoesNotExist,
            Status.PositionAndLetterOk,
            Status.LetterExistsInAnotherPosition,     
        };
        CollectionAssert.AreEqual(expected, current );
    }

   

}