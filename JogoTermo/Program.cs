﻿namespace JogoTermo;
public class Progrma
{
    public static void Main(string[] args)
    {
       Menu.GerarMenu();
       GerenciadorDoJogo game1 = new GerenciadorDoJogo();
       BancoDePalavras banco1 = new BancoDePalavras();
       banco1.SotearPalavra();
       BancoDePalavras.ArrayPalavraSorteada = banco1.PalavraSorteada.ToUpper().ToCharArray();
       int tentativas = 0;
       while(tentativas < 5)
       {
            System.Console.WriteLine("DIGITE SEU PALPITE");
            string palpite = Console.ReadLine().ToUpper();;
            char[] arrayPalpite = palpite.ToCharArray();
            List<Status> resultado =  game1.Jogar(palpite);
            if(ValidacaoResposta.VerificarPalpite(palpite) == Status.Sucesso)
            {
                for(int i = 0; i < resultado.Count; i++)
                { 
                   if( resultado[i] == Status.PositionAndLetterOk)
                   {
                      Console.ForegroundColor = ConsoleColor.Green;
                      System.Console.Write(arrayPalpite[i]);
                      Console.ForegroundColor = ConsoleColor.White;
                   }
                   if( resultado[i] == Status.LetterDoesNotExist)
                   {     
                       System.Console.Write(arrayPalpite[i]);    
                   }
                   if( resultado[i] == Status.LetterExistsInAnotherPosition)
                   {
                      Console.ForegroundColor = ConsoleColor.Red;
                      System.Console.Write(arrayPalpite[i]);
                      Console.ForegroundColor = ConsoleColor.White;
                   }
                }
               if(game1.VenceuOuPerdeu(resultado) == Status.Venceu)
               {
                  System.Console.WriteLine("\nPARABENS, VOCÊ VENCEU");
                  tentativas = 10;
               }
            }
           if(ValidacaoResposta.VerificarPalpite(palpite) == Status.DigiteUmaPalvra)
           {
               System.Console.WriteLine("PALAVRA INVALIDA, VOCÊ PERDEU UMA TENTATIVA"); 
           }
           if(ValidacaoResposta.VerificarPalpite(palpite) == Status.PalavraNaoPossueCincoLetras)
           {
               System.Console.WriteLine("PALAVRA NÃO POSSUE 5 LETRAS, VOCÊ PERDEU UMA TENTATIVA");
           }
           System.Console.WriteLine(" ");
           tentativas++;
       }   
       if(tentativas == 5)
       {
        System.Console.WriteLine("QUE PENA, VOCE PERDEU."); 
        System.Console.WriteLine("QA PALAVRA ERA: " + banco1.PalavraSorteada.ToUpper());
       }
    }
}


