namespace JogoTermo
{
    public class BancoDePalavras
    {
        public string PalavraSorteada{get; set;}
        public static char[] ArrayPalavraSorteada{get;set;}
        public static List<string> Palavras = new List<string>()
        {
            "perto",
            "corte",
            "ponte",
            "nobre",
            "sutil",
            "justo"
        };
        
        public void SotearPalavra()
        {
            Random r = new Random();
            int codigo = r.Next(0, Palavras.Count);

            PalavraSorteada = Palavras[codigo];
        }
    }
}