﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JogoTermo
{
    public class Menu
    {
        public static void GerarMenu()
        {
            System.Console.WriteLine("JOGO TERMO\n");
            System.Console.WriteLine("COR AZUL: POSIÇÃO E LETRA OK");
            System.Console.WriteLine("COR VERDE: LETRA OK, MAS EM OUTRA POSIÇÃO");
            System.Console.WriteLine("COR BRANCA: LETRA NAO EXISTE NA PALAVRA");
            System.Console.WriteLine("VOCÊ TEM 5 TENTATIVAS\n");
        }
    }
}
