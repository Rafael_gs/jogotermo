namespace JogoTermo;

public enum Status
{
    DigiteUmaPalvra,
    PalavraNaoPossueCincoLetras,
    Sucesso,
    PositionAndLetterOk,
    LetterDoesNotExist,
    LetterExistsInAnotherPosition,
    Venceu,
    Perdeu
}