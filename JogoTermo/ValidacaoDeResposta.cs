using System.Text.RegularExpressions;

namespace JogoTermo;

public class ValidacaoResposta
{
    
     public static Status VerificarPalpite(string palavra)
    {
        
        if (Regex.Match(palavra.ToUpper(), @"[A-Z]").Success == true)
        {
            if (palavra.Length != 5)
            {
                return Status.PalavraNaoPossueCincoLetras;
            }
            if(Regex.Match(palavra, @"[0-9]").Success == true)
            {
                return Status.DigiteUmaPalvra;
            }
            if(Regex.Match(palavra, @"[!@#\$%\^&\*\(\)]").Success == true)
            {
                return Status.DigiteUmaPalvra;
            }
            return Status.Sucesso;
        }
        return Status.DigiteUmaPalvra;
    }

}