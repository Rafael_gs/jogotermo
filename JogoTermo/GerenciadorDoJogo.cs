using System.Runtime.ConstrainedExecution;
using System.Text.RegularExpressions;

namespace JogoTermo;
public class GerenciadorDoJogo
{
    public string Palpite;
    public List<Status> PosicoesLetras{get; set;}
    public Status VenceuOuPerdeu(List<Status> status)
    {
        if (status[0] == Status.PositionAndLetterOk && status[1] == Status.PositionAndLetterOk &&
        status[2] == Status.PositionAndLetterOk && status[3] == Status.PositionAndLetterOk &&
        status[4] == Status.PositionAndLetterOk)

        {
            return Status.Venceu;
        }
        else
        {
            return Status.Perdeu;
        }
    }
    public List<Status> Jogar(string palpite)
    {
        Palpite = palpite;
        int contador = 0;
        PosicoesLetras = new List<Status>();

         if (ValidacaoResposta.VerificarPalpite(Palpite) == Status.Sucesso)
            {
                while(contador < 5)
                {
                    char[] arrayPalpite = Palpite.ToCharArray();
                    for (int i = 0; i < Palpite.Length; i++)
                    {
                       if(arrayPalpite[i] != BancoDePalavras.ArrayPalavraSorteada[i])
                       {
                        PosicoesLetras.Add(Status.LetterDoesNotExist);
                         for(int x = 0; x < Palpite.Length; x++)
                            {
                                if(arrayPalpite[i] == BancoDePalavras.ArrayPalavraSorteada[x])
                                {
                                    PosicoesLetras.RemoveAt(i);
                                    PosicoesLetras.Add(Status.LetterExistsInAnotherPosition);
                                }
                            }
                        contador++;
                       }
                       if(arrayPalpite[i] == BancoDePalavras.ArrayPalavraSorteada[i])
                       {
                            PosicoesLetras.Add(Status.PositionAndLetterOk);
                            contador++;
                       }
                    }                
                }
            }
        return PosicoesLetras;

    }

}






 

